/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronfabricaabstracta.fabrica;

/**
 *
 * @author ADMIN
 */
public class MasaQueso implements IElementoMasa{

    @Override
    public void producir() {
        System.out.println("Generando masa con bordes de queso");
    }

    @Override
    public String getDatos() {
        return "Masa redonda regular con bordes de queso";
    }
    
}
