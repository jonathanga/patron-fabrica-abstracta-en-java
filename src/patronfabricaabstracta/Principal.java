/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronfabricaabstracta;

import patronfabricaabstracta.fabrica.FabricaPizza;
import patronfabricaabstracta.fabrica.IElementoMasa;
import patronfabricaabstracta.fabrica.IElementoSabor;
import patronfabricaabstracta.fabrica.IFabrica;

/**
 *
 * @author ADMIN
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        IFabrica miFabrica = new FabricaPizza();
        
        miFabrica.crearElementos();
        IElementoMasa miMasa = miFabrica.getElementoMasa();
        IElementoSabor miSabor = miFabrica.getElementoSabor();
        System.out.println("Mi Pizza tiene: "+miMasa.getDatos()+miSabor.getInformacion());
    }
    
}
